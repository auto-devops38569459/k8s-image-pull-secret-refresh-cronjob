FROM alpine:3.19.0


WORKDIR /home/nonroot


RUN apk add --no-cache bash curl git aws-cli jq

RUN curl -LO https://dl.k8s.io/release/v1.30.0/bin/linux/amd64/kubectl
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
RUN rm kubectl


RUN addgroup nonroot
RUN adduser --disabled-password --gecos "" --ingroup nonroot nonroot


COPY RefreshCronjobScript.sh .


RUN chown -R nonroot:nonroot /home/nonroot
USER nonroot


CMD ["/home/nonroot/RefreshCronjobScript.sh"]
