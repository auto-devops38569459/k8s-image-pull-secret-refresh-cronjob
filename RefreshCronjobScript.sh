#!/usr/bin/env bash
set -e
[[ $TRACE ]] && set -x


DOCKER_PASSWORD=$(aws ecr get-login-password --region "$AWS_REGION")
DOCKER_SERVER=$(echo "$APPLICATION_IMAGE" | sed 's/\/.*//')

data=$(kubectl -n "$DEPLOY_NAMESPACE" create secret docker-registry "$IMAGE_PULL_SECRET_NAME" --docker-server=$DOCKER_SERVER --docker-username=AWS --docker-password=$DOCKER_PASSWORD --dry-run=client -ojson | jq -c ".data")
json="{\"data\": $data}"
kubectl -n "$DEPLOY_NAMESPACE" patch secret "$IMAGE_PULL_SECRET_NAME" --patch "$json"
